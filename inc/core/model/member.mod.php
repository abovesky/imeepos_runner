<?php

/**
 * used: 
 * User: imeepos
 * Qq: 1037483576
 */
class member
{
    public $table = 'imeepos_runner3_member';

    public function __construct()
    {
        $this->install();
    }

    public function getall(){
        global $_W;
        $list = pdo_getall($this->table,array('uniacid'=>$_W['uniacid']));
        return $list;
    }

    public function delete($id){
        if(empty($id)){
            return '';
        }
        pdo_delete($this->table,array('id'=>$id));
    }
	public function getList2($page,$where =""){
		global $_W;
		if(empty($page)){
			$page = 1;
		}
		$psize = 5;
		$total = 0;
		$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where} ORDER BY time DESC limit ".(($page-1)*$psize).",".$psize;
		$params = array(':uniacid'=>$_W['uniacid']);
		$result = array();
		$result['list'] = pdo_fetchall($sql,$params);
		$sql = "SELECT COUNT(*) FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where}";
		$total = pdo_fetchcolumn($sql,$params);

		$result['pager'] = pagination($total, $page, $psize);
		if(empty($result)){
			return array();
		}else{
			return $result;
		}
	}
    public function getList($page,$where =""){
        global $_W;
        if(empty($page)){
            $page = 1;
        }
        $psize = 20;
        $total = 0;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where} ORDER BY time DESC limit ".(($page-1)*$psize).",".$psize;
        $params = array(':uniacid'=>$_W['uniacid']);
        $result = array();
        $result['list'] = pdo_fetchall($sql,$params);
        $sql = "SELECT COUNT(*) FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where}";
        $total = pdo_fetchcolumn($sql,$params);

        $result['pager'] = pagination($total, $page, $psize);
        if(empty($result)){
            return array();
        }else{
            return $result;
        }
    }
    public function update($data){
        global $_W;
        $data['uniacid'] = $_W['uniacid'];
        if(empty($data['id'])){
            pdo_insert($this->table,$data);
            $data['id'] = pdo_insertid();
        }else{
            //更新
            pdo_update($this->table,$data,array('uniacid'=>$_W['uniacid'],'id'=>$data['id']));
        }
        return $data;
    }
	public function getInfo($openid){
		global $_W;
		pdo_delete($this->table,array('openid'=>''));
		pdo_delete($this->table,array('avatar'=>''));
		if(is_numeric($openid)){
			$uid = $openid;
			$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND uid = :uid";
			$params = array(':uniacid'=>$_W['uniacid'],':uid'=>$uid);
			$item = pdo_fetch($sql,$params);
			return $item;
		}else{
			$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND openid = :openid";
			$params = array(':uniacid'=>$_W['uniacid'],':openid'=>$openid);
			$item = pdo_fetch($sql,$params);
			return $item;
		}
	}
	public function getRunnerList(){
		global $_W;
		$sql = "SELECT * FROM ".tablename('imeepos_runner3_member')." WHERE uniacid = :uniacid AND isrunner = :isrunner";
		$params = array(':uniacid'=>$_W['uniacid'],':isrunner'=>1);
		$list = pdo_fetchall($sql,$params);
		return $list;
	}
	public function install(){
		if(!pdo_tableexists($this->table)){
			$sql = "CREATE TABLE ".tablename($this->table)." (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `uid` int(11) unsigned NOT NULL,
			  `uniacid` int(11) unsigned NOT NULL,
			  `status` tinyint(2) unsigned NOT NULL,
			  `groupid` int(11) unsigned NOT NULL,
			  `time` int(11) DEFAULT NULL,
			  `openid` varchar(64) DEFAULT NULL,
			  `online` tinyint(2) DEFAULT '0',
			  `nickname` varchar(32) DEFAULT '',
			  `avatar` varchar(320) DEFAULT NULL,
			  `gender` tinyint(2) DEFAULT '0',
			  `city` varchar(32) DEFAULT '',
			  `provice` varchar(32) DEFAULT '',
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8";
			pdo_query($sql);
		}

		if(!pdo_fieldexists($this->table,'realname')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `realname` varchar(32) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'mobile')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `mobile` varchar(32) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'xinyu')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `xinyu` int(11) DEFAULT '0'");
		}
		if(!pdo_fieldexists($this->table,'isrunner')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `isrunner` tinyint(2) DEFAULT '0'");
		}
		if(!pdo_fieldexists($this->table,'card_image1')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `card_image1` varchar(320) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'card_image2')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `card_image2` varchar(320) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'cardnum')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `cardnum` varchar(64) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'lat')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `lat` varchar(64) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'lng')){
			pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `lng` varchar(64) DEFAULT ''");
		}
		if(!pdo_fieldexists($this->table,'status')){
			$sql = "ALTER TABLE ".tablename($this->table)." ADD COLUMN `status` tinyint(4) DEFAULT '1'";
			pdo_query($sql);
		}
		if(!pdo_fieldexists($this->table,'nickname')){
			$sql = "ALTER TABLE ".tablename($this->table)." ADD COLUMN `nickname` varchar(64) DEFAULT ''";
			pdo_query($sql);
		}
		if(!pdo_fieldexists($this->table,'uid')){
			$sql = "ALTER TABLE ".tablename($this->table)." ADD COLUMN `uid` int(11) DEFAULT '0'";
			pdo_query($sql);
		}
	}
}