<?php

/**
 * used: 
 * User: imeepos
 * Qq: 1037483576
 */
class paylog
{
    public $table = 'imeepos_runner3_paylog';

    public function __construct()
    {
        $this->install();
    }

    public function getall(){
        global $_W;
        $list = pdo_getall($this->table,array('uniacid'=>$_W['uniacid']));
        return $list;
    }

    public function delete($id){
        if(empty($id)){
            return '';
        }
        pdo_delete($this->table,array('id'=>$id));
    }

    public function clear(){
        global $_W;
        
    }

    public function getList($page,$where =""){
        global $_W;
        $this->clear();
        
        if(empty($page)){
            $page = 1;
        }
        $psize = 20;
        $total = 0;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where} ORDER BY time DESC limit ".(($page-1)*$psize).",".$psize;
        $params = array(':uniacid'=>$_W['uniacid']);
        $result = array();
        $result['list'] = pdo_fetchall($sql,$params);
        $sql = "SELECT COUNT(*) FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where}";
        $total = pdo_fetchcolumn($sql,$params);

        $result['pager'] = pagination($total, $page, $psize);
        if(empty($result)){
            return array();
        }else{
            return $result;
        }
    }
    public function getInfoByOrdersn($ordersn){
        global $_W;
        $sql = "SELECT * FROM ".tablename('imeepos_runner3_paylog')." WHERE uniacid = :uniacid AND tid = :tid";
        $par = array(":uniacid"=>$_W['uniacid'],':tid'=>$ordersn);
        $paylog = pdo_fetch($sql,$par);
        return $paylog;
    }
    public function payResultAddShangJin($ordersn){
        global $_W;
        $paylog = $this->getInfoByOrdersn($ordersn);
        $data = unserialize($paylog['setting']);
        if(!empty($data['id'])){
            M('tasks')->update($data);
            return true;
        }
        return false;
    }
    public function autoUpdate($data){
        global $_W;
        $data['tid'] = "U".time().random(6,true);
        $data['uniacid'] = $_W['uniacid'];
        $data['status'] = 0;
        $data['openid'] = $_W['openid'];
        $data['time'] = time();
        $return = $this->update($data);
        return $return;
    }
    public function update($data){
        global $_W;
        $data['uniacid'] = $_W['uniacid'];
        if(empty($data['id'])){
            pdo_insert($this->table,$data);
            $data['id'] = pdo_insertid();
        }else{
            //更新
            pdo_update($this->table,$data,array('uniacid'=>$_W['uniacid'],'id'=>$data['id']));
        }
        return $data;
    }
    public function getInfo($id){
        global $_W;
        $item = pdo_get($this->table,array('id'=>$id));
        return $item;
    }
    public function install(){
        if(!pdo_tableexists($this->table)){
            $sql = "CREATE TABLE ".tablename($this->table)." (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `uniacid` int(11) DEFAULT '0',
              `tid` varchar(64) DEFAULT '',
              `time` int(11) DEFAULT '0',
              `setting` text,
              `status` tinyint(2) DEFAULT '0',
              `openid` varchar(64) DEFAULT '',
              `fee` float(10,2) DEFAULT '0.00',
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8";
            pdo_query($sql);
        }
        if(!pdo_fieldexists($this->table,'fee')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `fee` float(10,2) DEFAULT '0.00'");
        }
        if(!pdo_fieldexists($this->table,'type')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `type` varchar(32) DEFAULT ''");
        }
        if(!pdo_fieldexists($this->table,'taskid')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `taskid` int(10) DEFAULT '0'");
        }
    }
}