<?php
class setting{
	public $table = 'imeepos_runner3_setting';
	
	public function __construct(){
		$this->install();
	}
	
	function getByCode($code){
		global $_W;
		$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND code = :code";
		$params = array(':uniacid'=>$_W['uniacid'],':code'=>$code);
		$item = pdo_fetch($sql,$params);
		return $item;
	}
	
	function getValue($code){
		global $_W;
		$sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid AND code = :code";
		$params = array(':uniacid'=>$_W['uniacid'],':code'=>$code);
		$item = pdo_fetch($sql,$params);
		if(!empty($item)){
			return iunserializer($item['value']);
		}else{
			return array();
		}
	}
	
	function update($code,$value){
		global $_W;
		$item = $this->getByCode($code);
		if(empty($item)){
			pdo_insert($this->table,array('uniacid'=>$_W['uniacid'],'code'=>$code,'value'=>$value));
		}else{
			pdo_update($this->table,array('value'=>$value),array('uniacid'=>$_W['uniacid'],'code'=>$code));
		}
	}
	
	public function install(){
		if(!pdo_tableexists($this->table)){
			$sql = "CREATE TABLE ".tablename($this->table)." (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `uniacid` int(11) DEFAULT '0',
			  `code` varchar(32) DEFAULT '',
			  `value` text,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8";
			pdo_query($sql);
		}
	}
}