<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/5/30
 * Time: 13:30
 */
/**
 * used: 
 * User: imeepos
 * Qq: 1037483576
 */

class tasks
{
    public $table = 'imeepos_runner3_tasks';

    public function __construct()
    {
        $this->install();
    }

    public function getall(){
        global $_W;
        $list = pdo_getall($this->table,array('uniacid'=>$_W['uniacid']));
        return $list;
    }

    public function delete($id){
        if(empty($id)){
            return '';
        }
        pdo_delete($this->table,array('id'=>$id));
    }

    function check(){
        global $_W;

    }

    public function clear(){
        global $_W;
        //删除过期未支付订单
        $code = 'plugin_setting';
        $item = M('setting')->getByCode($code);
        $plugin = iunserializer($item['value']);
        $web_delete_time = floatval($web_delete_time);

        $web_delete_time = 60*60*$web_delete_time;
        $time = time()-$web_delete_time;

        $sql = "DELETE FROM ".tablename($this->table)." WHERE status = 0 AND time < :time AND uniacid = :unaicid";
        $params = array(':time'=>$time,':uniacid'=>$_W['uniacid']);
        pdo_query($sql,$params);

        $sql = "DELETE FROM ".tablename($this->table)." WHERE status = 0 AND create_time < :create_time AND uniacid = :unaicid";
        $params = array(':create_time'=>$time,':uniacid'=>$_W['uniacid']);
        pdo_query($sql,$params);
    }

    public function getList($page,$where ="",$params = array()){
        global $_W;
        if(empty($page)){
            $page = 1;
        }
        $psize = 20;
        $total = 0;
        $sql = "SELECT * FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where} ORDER BY create_time DESC limit ".(($page-1)*$psize).",".$psize;
        $params['uniacid'] = $_W['uniacid'];
        $result = array();
        $result['list'] = pdo_fetchall($sql,$params);
        $sql = "SELECT COUNT(*) FROM ".tablename($this->table)." WHERE uniacid = :uniacid {$where} ";
        $total = pdo_fetchcolumn($sql,$params);
        $result['pager'] = pagination($total, $page, $psize);

        
        if(empty($result)){
            return array();
        }else{
            return $result;
        }
    }

    public function update($data){
        global $_W;
        $data['uniacid'] = $_W['uniacid'];
        if(empty($data['id'])){
            pdo_insert($this->table,$data);
            $data['id'] = pdo_insertid();
        }else{
            //更新
            pdo_update($this->table,$data,array('uniacid'=>$_W['uniacid'],'id'=>$data['id']));
        }
        return $data;
    }
    public function addReadNum($id){
        $item = $this->getInfo($id);
        $read_num = $item['read_num'];
        pdo_update($this->table,array('read_num'=>$read_num+1),array('id'=>$id));
    }
    public function addShareNum($id){
        $item = $this->getInfo($id);
        $share_num = $item['share_num'];
        pdo_update($this->table,array('share_num'=>$share_num+1),array('id'=>$id));
    }
    public function addListenNum($id){
        $item = $this->getInfo($id);
        $listen_num = $item['listen_num'];
        pdo_update($this->table,array('listen_num'=>$listen_num+1),array('id'=>$id));
    }
    public function getInfo($id){
        global $_W;
        $task = pdo_get($this->table,array('id'=>$id));
        if($task['type'] == 0 || $task['type'] == 1){
            $sql = "SELECT * FROM ".tablename('imeepos_runner3_detail')." WHERE taskid = :taskid";
            $params = array(':taskid'=>$task['id']);
            $detail = pdo_fetch($sql,$params);
            $detail['total'] = $detail['total'];
        }else{
            $sql = "SELECT * FROM ".tablename('imeepos_runner3_buy')." WHERE taskid = :taskid";
            $params = array(':taskid'=>$task['id']);
            $detail = pdo_fetch($sql,$params);
            $detail['total'] = $detail['freight'];
        }
        $task['detail'] = $detail;
        return $task;
    }
    public function install(){
        if(!pdo_tableexists($this->table)){
            $sql = "CREATE TABLE ".tablename($this->table)." (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `uniacid` int(11) DEFAULT '0',
              `status` tinyint(2) DEFAULT '1',
              `create_time` int(11) DEFAULT '0',
              `cityid` int(11) DEFAULT '0',
              `media_id` varchar(132) DEFAULT '',
              `openid` varchar(64) DEFAULT '',
              `desc` text,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8";
            pdo_query($sql);
        }
        if(!pdo_fieldexists($this->table,'total')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `total` float(10,2) DEFAULT '0.00'");
        }
        if(!pdo_fieldexists($this->table,'small_money')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `small_money` float(10,2) DEFAULT '0.00'");
        }
        if(!pdo_fieldexists($this->table,'limit_time')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `limit_time` int(11) DEFAULT '0'");
        }
        if(!pdo_fieldexists($this->table,'address')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `address` varchar(320) DEFAULT ''");
        }
        if(!pdo_fieldexists($this->table,'city')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `city` varchar(32) DEFAULT ''");
        }
        if(!pdo_fieldexists($this->table,'desc')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `desc` text");
        }
        if(!pdo_fieldexists($this->table,'type')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `type` tinyint(4) DEFAULT '0'");
        }
        if(!pdo_fieldexists($this->table,'update_time')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `update_time` int(11) DEFAULT '0'");
        }
        if(!pdo_fieldexists($this->table,'code')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `code` varchar(64) DEFAULT ''");
        }
        if(!pdo_fieldexists($this->table,'qrcode')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `qrcode` text");
        }
        if(!pdo_fieldexists($this->table,'read_num')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `read_num` int(11) DEFAULT '0'");
        }
        if(!pdo_fieldexists($this->table,'share_num')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `share_num` int(11) DEFAULT '0'");
        }
        if(!pdo_fieldexists($this->table,'listen_num')){
            pdo_query("ALTER TABLE ".tablename($this->table)." ADD COLUMN `listen_num` int(11) DEFAULT '0'");
        }
    }
}