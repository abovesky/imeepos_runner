<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/5/28
 * Time: 14:35
 */

global $_W,$_GPC;
$set = M('setting')->getValue('pay');
if($_W['ispost']){
    $data = array();
    $data['pay']  = is_array($_GPC['pay']) ? $_GPC['pay'] : array();
    $data['cert'] = $this->upload_cert('weixin_cert_file');
    $data['key']  = $this->upload_cert('weixin_key_file');
    $data['root'] = $this->upload_cert('weixin_root_file');
    M('setting')->update('pay',iserializer($data));

    message('保存成功','',success);
}
include $this->template('payset');