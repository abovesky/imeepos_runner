<?php
global $_W,$_GPC;
include MODULE_ROOT.'/inc/web/__init.php';

$_GPC['do'] = 'plugin_list';

$plugins = array();
$plugins[] = array(
		'title'=>'帮助文档',
		'code'=>'help',
		'desc'=>'帮助文档',
		'author'=>$_W['account']['name'],
		'image'=>MODULE_URL.'plugin/help/icon.jpg'
);
$plugins[] = array(
		'title'=>'基础设置',
		'code'=>'setting',
		'desc'=>'控制各插件是否开启',
		'author'=>$_W['account']['name'],
		'image'=>MODULE_URL.'plugin/setting/icon.jpg'
);
include $this->template('plugin_list');