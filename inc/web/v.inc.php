<?php 
global $_W,$_GPC;
$act = trim($_GPC['act']);
$act = !empty($act)?$act:'list';

$_GPC['do'] = 'v';

if($act == 'list'){
	$list = M('member')->getRunnerList();
	foreach ($list as &$li){
		if($li['status'] == 0){
			$li['statustitle'] = '待审核';
			$li['status_label'] = 'label-danger';
		}else{
			$li['statustitle'] = '审核通过';
			$li['status_label'] = 'label-info';
		}
		if($li['isrunner'] == 0){
			$li['isrunnertitle'] = '否';
			$li['isrunner_label'] = 'label-danger';
		}else{
			$li['isrunnertitle'] = '是';
			$li['isrunner_label'] = 'label-info';
		}
	}
}

if($act == 'status'){
	$data = array();
	$input = $_GPC['__input'];
	$data['status'] = $input['status'];
	$data['id'] = $input['id'];
	if($data['status'] == 1){
		$content = "";
		$content = "恭喜您，您的跑腿资料已审核通过！~\n";
		$content .= "时间：".date('Y年m月d日 h点i分',time())."\n";
		$content .= "咚咚咚，您的跑腿资料已审核通过，立即前往接单~";
		$url = $_W['siteroot'].'app/'.$this->createMobileUrl('tasks');
		$retrun = mc_notice_consume2($input['openid'], '跑腿资料已审核通过通知', $content, $url,'');
	}else{
		$content = "";
		$content = "很抱歉，您的跑腿资料审核失败！~\n";
		$content .= "时间：".date('Y年m月d日 h点i分',time())."\n";
		$content .= "咚咚咚，您的跑腿资料审核失败，请重新提交~";
		$url = $_W['siteroot'].'app/'.$this->createMobileUrl('v');
		$retrun = mc_notice_consume2($input['openid'], '跑腿资料审核失败通知', $content, $url,'');
	}
	M('member')->update($data);
	die();
}

if($act == 'add'){
	$data = array();
	$input = $_GPC['__input'];
	$data['status'] = intval($input['status']);
	$data['isrunner'] = intval($input['isrunner']);
	$data['xinyu'] = intval($input['xinyu']);
	$data['id'] = $input['id'];
	M('member')->update($data);
	die();
}

if($act == 'xinyu'){
	$input = $_GPC['__input'];
	$id = intval($input['id']);
	$xinyu = floatval($input['xinyu']);
	$kouchu = floatval($input['kouchu']);

	if($xinyu > 0){
		$sql = "SELECT * FROM ".tablename('imeepos_runner3_member')." WHERE id = :id";
		$params = array(':id'=>$id);
		$user = pdo_fetch($sql,$params);
		
		$ret_xinyu = floatval($user['xinyu']) + $xinyu;
		pdo_update('imeepos_runner3_member',array('xinyu'=>$ret_xinyu),array('id'=>$input['id']));

		$ret = array();
		$ret['message'] = '充值成功';
		die(json_encode($data));
	}
	
	if($kouchu > 0){
		$sql = "SELECT * FROM ".tablename('imeepos_runner3_member')." WHERE id = :id";
		$params = array(':id'=>$id);
		$user = pdo_fetch($sql,$params);
		
		$ret_xinyu = floatval($user['xinyu']) - $kouchu;
		pdo_update('imeepos_runner3_member',array('xinyu'=>$ret_xinyu),array('id'=>$input['id']));

		$ret = array();
		$ret['message'] = '扣除成功';
		die(json_encode($data));
	}
	
}

if($act == 'delete'){
	$id = intval($_GPC['id']);
	pdo_delete('imeepos_runner3_member',array('id'=>$id));
	die();
}

include $this->template('web/task/v');
