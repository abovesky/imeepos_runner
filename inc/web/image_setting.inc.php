<?php
global $_W,$_GPC;
include MODULE_ROOT.'/inc/web/__init.php';

$act = trim($_GPC['act']);
$act = !empty($act)?$act:'list';

$imagesModel = M('image');
$table = 'imeepos_runner3_image';

if($act == 'list'){
	$list = $imagesModel->fetchall(array());
}

if($act == 'add'){
	$data = array();
	$post = $_GPC['__input'];
	$data = $post;
	$data['src'] = tomedia($post['src']);

	$data['code'] = trim($post['code']);

	if(!empty($post['id'])){
		pdo_update($table,$data,array('id'=>$post['id']));
		$data['id'] = $post['id'];
	}else{
		$data['uniacid'] = $_W['uniacid'];
		pdo_insert($table,$data);
		$data['id'] = pdo_insertid();
	}
	die(json_encode(array('data'=>$data)));
}


if($act == 'delete'){
	$id = intval($_GPC['fid']);
	pdo_delete($table,array('id'=>$id));
}
include $this->template('web/template/image');