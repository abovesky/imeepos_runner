<?php 
global $_W,$_GPC;
$act = trim($_GPC['act']);
$act = !empty($act)?$act:'list';
$_GPC['do'] = 'task';

if($act == 'list'){
	$where = "";
	$params = array(':uniacid'=>$_W['uniacid']);
	if(!isset($_GPC['status'])){

	}else{
		$where = " AND status = :status ";
		$params[':status'] = intval($_GPC['status']);
	}
	if(!empty($_GPC['openid'])){
		$where .=" AND openid = :openid";
		$params[':openid'] = trim($_GPC['openid']);
	}

	$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE uniacid = :uniacid {$where} ORDER BY create_time DESC";
	$list = pdo_fetchall($sql,$params);

	if(!empty($list)){
		foreach ($list as &$li){
			$li['icon'] = tomedia($li['icon']);
			$li['start_time'] = date('Y-m-d',$li['start_time']);
			$li['end_time'] = date('Y-m-d',$li['end_time']);

			$sql = "SELECT * FROM ".tablename('imeepos_runner3_listenlog')." WHERE taskid = :taskid";
			$params = array(':taskid'=>$li['id']);
			$li['in_num'] = pdo_fetchcolumn($sql,$params);

			$user = M('member')->getInfo($li['openid']);

			$sql = "SELECT * FROM ".tablename('mc_mapping_fans')." WHERE uniacid = :uniacid AND openid = :openid";
			$params = array(':uniacid'=>$_W['uniacid'],':openid'=>$li['openid']);
			$fans = pdo_fetch($sql,$params);
			$user['fansid'] = $fans['fanid'];

			$li['user'] = $user;
			if($li['status'] == 0){
				$li['statustitle'] = '未支付';
				$li['status_label'] = 'label-danger';
			}else if($li['status'] == 1){
				$li['statustitle'] = '等待接单';
				$li['status_label'] = 'label-warning';
			}else if($li['status'] == 2){
				$li['statustitle'] = '已受理';
				$li['status_label'] = 'label-default';
			}else if($li['status'] == 3){
				$li['statustitle'] = '已完成';
				$li['status_label'] = 'label-primary';
			}else if($li['status'] == 4){
				$li['statustitle'] = '已结款';
				$li['status_label'] = 'label-info';
			}

			if($li['type'] == 0){
				$li['typetitle'] = '帮我送';
				$li['type_label'] = 'label-danger';
			}else if($li['type'] == 1){
				$li['typetitle'] = '预约取';
				$li['type_label'] = 'label-warning';
			}else{
				$li['typetitle'] = '帮我买';
				$li['type_label'] = 'label-info';
			}
		}
		unset($li);
	}
}

if($act == 'edit'){
	$id = intval($_GPC['id']);
	$item = M('tasks')->getInfo($id);
	include $this->template('task_edit');
	exit();
}

if($act == 'edit_item'){
	$id = intval($_GPC['id']);
	$item = M('tasks')->getInfo($id);
	$item = $item['detail'];
	$item['images'] = unserialize($item['images']);
	
	include $this->template('task_edit_item');
	exit();
}

$input = $_GPC['__input'];
if($act == 'search_by_keyword'){
	$keyword = $input['keyword'];
	$where = " AND isrunner = 1";
	if(!empty($keyword)){
		$where .= " (AND nickname like '%{$keyword}%' OR mobile like '%{$keyword}%' OR realname like '%{$keyword}%')";
	}
	$list = M('member')->getList(1,$where);
	die(json_encode($list['list']));
}

if($act == 'delete'){
	$id = intval($_GPC['id']);
	pdo_delete('imeepos_runner3_tasks',array('id'=>$id));
}
include $this->template('web/task/task');