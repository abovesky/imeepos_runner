<?php
/*ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);
*/
global $_W,$_GPC;

include MODULE_ROOT.'/inc/mobile/__init.php';

$id = intval($_GPC['loc_id']);
$loc = $_GPC['loc'];

$latlng = $loc['latlng'];
unset($loc['latlng']);
unset($loc['module']);
if(!empty($id)){
	$loc['id'] = $id;
}
$loc['lat'] = $latlng['lat'];
$loc['lng'] = $latlng['lng'];

$return = M('address')->updateAddress($loc);

die(json_encode($return));

$url = $this->createMobileUrl('home_address');
header("location:$url");
exit();
