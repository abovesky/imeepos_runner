<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$page = 1;
$psize = 20;

$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE uniacid = :uniacid AND status = :status order by create_time DESC limit ".($page-1)*$psize.",".$psize;
$params = array(':uniacid'=>$_W['uniacid'],':status'=>1);
$orders = pdo_fetchall($sql,$params);

foreach ($orders as &$order){
	$uid = mc_openid2uid($order['openid']);
	$order['user'] = mc_fetch($uid);
	$order['text'] = $order['desc'];
	$order['status'] = $order['status'];
	$order['create_time'] = date('Y-m-d h:i',$order['create_time']);
}

$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/task/all';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}