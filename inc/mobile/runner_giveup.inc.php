<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$id = intval($_GPC['reciveid']);

$recive = M('recive')->getInfo($id);
$taskid = $recive['taskid'];

$task = M('tasks')->getInfo($taskid);

if(!empty($task)){
	if($task['status'] == 2){
		$runner = M('member')->getInfo($_W['openid']);
		$xinyu = intval($runner['xinyu']);
		//获取后台设置
		$item = M('setting')->getValue('v_set');
		
		$kouchu = intval($item['giveup_num']);
		$shengyu = $xinyu - $kouchu;
		pdo_update('imeepos_runner3_member',array('xinyu'=>$xinyu-$kouchu),array('id'=>$runner['id']));
		pdo_delete('imeepos_runner3_recive',array('id'=>$id));
		pdo_update('imeepos_runner3_tasks',array('status'=>1),array('id'=>$task['id']));
		
		//发送消息 发给取消任务的人
		$content = "";
		$content = "放弃订单成功！~\n";
		$content .= "时间：".date('Y年m月d日 h点i分',time())."\n";
		$content .= "您的信誉度-".$kouchu."，剩余".$shengyu."，继续前往赚钱";
		$url = $_W['siteroot'].'app/'.$this->createMobileUrl('tasks');
		$retrun = mc_notice_consume2($_W['openid'], '取消任务通知', $content, $url,'');

		//发送消息 发给任务主
		$content = "";
		$content = "很抱歉通知您，您的任务服务人员【".$runner['realname']."】已被放弃！~\n";
		$content .= "时间：".date('Y年m月d日 h点i分',time())."\n";
		$content .= "扣除他信誉【-".$kouchu."】，请耐心等待附近服务人员接单，或增加赏金减少丢单率";
		$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$taskid));
		$retrun = mc_notice_consume2($task['openid'], '服务人员放弃任务通知', $content, $url,'');
		
		//可以放弃 扣除相应信誉
		$return = array();
		$return['result'] = 0;
		$return['message'] = '放弃订单成功，您的信誉度-'.$kouchu.'，剩余'.$shengyu.'，点击确定，前往充值';
		die(json_encode($return));
		
	}else if($task['status'] == 4){
		$sql = "SELECT * FROM ".tablename('imeepos_runner3_moneylog')." WHERE reciveid = :reciveid";
		$params = array(':reciveid'=>$reciveid);
		$money = pdo_fetch($sql,$params);
		$return = array();
		$return['result'] =1;
		$return['tid'] = $money['id'];
		$return['message'] = "此订单已完结，不可放弃！";
		die(json_encode($return));
	}else if($task['status'] == 3){
		$return = array();
		$return['result'] = 0;
		$return = array();
		$return['result'] =1;
		$return['tid'] = $money['id'];
		$return['message'] = "等待任务主结款中，不可放弃！";
		die(json_encode($return));
	}
}else{
	$return = array();
	$return['result'] =3;
	$return['message'] = "订单不存在或已删除！请核实！";
	die(json_encode($return));
}

$return = array();
$return['result'] =3;
$return['message'] = "系统错误，请联系站点管理员！";
die(json_encode($return));