<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$id = intval($_GPC['id']);

$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id = :id";
$params = array(':id'=>$id);
$task = pdo_fetch($sql,$params);

//recive

$sql = "SELECT * FROM ".tablename('imeepos_runner3_listenlog')." WHERE taskid = :taskid ORDER BY create_time DESC limit 10";
$params = array(':taskid'=>$id);
$listens = pdo_fetchall($sql,$params);

if(empty($listens)){
	$data = array();
	$data['status'] = 1;
	$data['data'] = $listens;
}else{
	foreach ($listens as &$li){
		$user = mc_fetch($li['openid']);
		$li['avatar'] = $user['avatar'];
		$li['nickname'] = $user['nickname'];
		$li['create_time'] = date('m-d h:i',$li['create_time']);
	}
	$data = array();
	$data['status'] = 0;
	$data['data'] = $listens;
}
die(json_encode($data));