<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$mobile = trim($_GPC['mobile']);

if(empty($_W['openid'])){
	message('openid为空，配置错误或在非微信浏览器中打开',referer(),'error');
}
$item = M('setting')->getValue('sms_set');
if(!empty($mobile)){
	//检查手机号是否已注册
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_member')." WHERE uniacid = :uniacid AND mobile = :mobile";
	$params = array(':uniacid'=>$_W['uniacid'],':mobile'=>$mobile);
	$membersss = pdo_fetch($sql,$params);
	
	if(!empty($membersss) && $membersss['openid'] != $_W['openid']){
		$result = array();
		$result['status'] = 0;
		$result['message'] = '您填写的手机号码已被注册，请重新输入！';
		die(json_encode($result));
	}
	$code = random(4,true);
	$content = "欢迎您".$user['nickname'].",您的验证码是:".$code;
	$data = array();
	$data['uniacid'] = $_W['uniacid'];
	$data['openid'] = $_W['openid'];
	$data['time'] = time();
	$data['code'] = $code;
	$data['mobile'] = $mobile;
	$data['content'] = $content;
	pdo_insert('imeepos_runner3_code',$data);
	$codeid = pdo_insertid();
	include IA_ROOT."/addons/imeepos_runner/inc/mobile/TopSdk.php";
	
	$c = new TopClient();
	$c->appkey = $item['appkey'];//appkey
	$c->secretKey = $item['appsecret'];//appsecret
	$req = new AlibabaAliqinFcSmsNumSendRequest();
	$req->setExtend("123456");
	$req->setSmsType("normal");
	$req->setSmsFreeSignName($item['signname']);//身份验证签名 身份验证
	
	$json = json_encode(array("code"=>$data['code'],'product'=>'跑腿'));
	$req->setSmsParam($json);
	$req->setRecNum($mobile);
	
	$req->setSmsTemplateCode($item['moban_num']);//  身份验证 模板号 SMS_6290144
	
	$return = $c->execute($req);
	
    $result = array();
    $result['codeid'] = $codeid;
    $result['result'] = $return;
    die(json_encode($result));
}
