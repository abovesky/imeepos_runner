<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$title = "完成订单";
//订单编号
$id = intval($_GPC['id']);
load()->model('mc');

$sql = "SELECT * FROM ".tablename('imeepos_runner3_recive')." WHERE taskid = :taskid";
$params = array(':taskid'=>$id);
$recive = pdo_fetch($sql,$params);

$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id = :id";
$params = array(':id'=>$id);
$task = pdo_fetch($sql,$params);

if(!empty($task)){
	if($task['status'] == 2 || $task['status'] == 3){
		//用户改变订单任务状态
		$fee = floatval($task['total']);
		//打钱到跑腿余额
		//扣除平台佣金
		$plugin_setting = M('setting')->getValue('plugin_setting');
		$platform_money = intval($plugin_setting['platform_money']);
		if(empty($platform_money) || $platform_money > 100){
			$platform_money = 100;
		}
		$fee = $fee * ($platform_money / 100 );

		$uid = mc_openid2uid($recive['openid']);
		mc_credit_update($uid, 'credit2',$fee,array($uid, '跑腿佣金', 0, 0));
		//插入记录表
		$data = array();
		$data['uniacid'] = $_W['uniacid'];
		$data['openid'] = $_W['openid'];
		$data['create_time'] = time();
		$data['reciveid'] = $recive['id'];
		$data['fee'] = $fee;

		$sql = "SELECT * FROM ".tablename('imeepos_runner3_moneylog')." WHERE reciveid = :reciveid";
		$params = array(':reciveid'=>$recive['id']);
		$m = pdo_fetch($sql,$params);
			
		if(!empty($m)){
			//赏金到账通知
			$content = "";
			$content = "恭喜您，".$fee."赏金已到账！~\n";
			$content .= "订单编号：".$tid."\n";
			$content .= "时间：".date('Y年m月d日 h点i分')."\n";
			$content .= "咚咚咚，恭喜您，恭喜您，任务完成!赏金".$fee."元已到账余额，请注意查收~，点击继续赚钱";
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('index');
			$retrun = mc_notice_consume2($_W['openid'], '赏金到账通知', $content, $url,'');
			$data = array();
			$data['result'] = 1;
			$data['message'] = '恭喜您，任务完成!赏金'.$fee.'元已到账余额，请注意查收';
			die(json_encode($data));
		}else{
			$content = "";
			$content = "恭喜您，".$fee."赏金已到账！~\n";
			$content .= "订单编号：".$tid."\n";
			$content .= "时间：".date('Y年m月d日 h点i分')."\n";
			$content .= "咚咚咚，恭喜您，恭喜您，任务完成!赏金".$fee."元已到账余额，请注意查收~，点击继续赚钱";
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('index');
			$retrun = mc_notice_consume2($recive['openid'], '赏金到账通知', $content, $url,'');
				
			pdo_insert('imeepos_runner3_moneylog',$data);

			pdo_update('imeepos_runner3_tasks',array('status'=>4,'update_time'=>time()),array('id'=>$task['id']));

			$data = array();
			$data['result'] = 1;
			$data['message'] = '恭喜您，任务完成!赏金'.$fee.'元已到账余额，请注意查收';
			die(json_encode($data));
		}
		$return = array();
		$return['result'] = 0;
		$return['message'] = "恭喜您操作成功！";
		die(json_encode($return));
	}else if($task['status'] == 4){
		$return = array();
		$return['result'] =2;
		$return['message'] = '此订单已打款，请确认！';
		die(json_encode($return));
	}else if($task['status'] == 0){
		$return = array();
		$return['result'] =2;
		$return['message'] = '此订单未支付成功，请确认！';
		die(json_encode($return));
	}else{
		$return = array();
		$return['result'] =2;
		$return['message'] = '此订单暂无人受理，请确认！';
		die(json_encode($return));
	}
}else{
	$return = array();
	$return['result'] =3;
	$return['message'] = '订单不存在或已删除！';
	die(json_encode($return));
}