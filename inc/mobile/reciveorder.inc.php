<?php 
include MODULE_ROOT.'/inc/mobile/common/global.func.php';
global $_W,$_GPC;

$id = intval($_GPC['id']);

$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id = :id";
$params = array(':id'=>$id);
$task = pdo_fetch($sql,$params);

load()->model('mc');
$runner = mc_fetch($_W['openid']);

if($task['openid'] == $_W['openid']){
	$return = array();
	$return['success'] = 1;
	$return['message'] = '这是您自己的任务哦！';

	$content = "";
	$content = "系统提醒，您有一个任务已过期~\n";
	$content .= "时间：".date('Y年m月d日 h点i分')."\n";
	$content .= "咚咚咚，您有一个任务已过期，请及时延时或者加价!";
	$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$task['id']));
	mc_notice_consume2($task['openid'], '系统提醒', $content, $url,'');

	die(json_encode($return));
}
$task['limit_time'] = intval($task['limit_time']);
if($task['limit_time'] < time()){
	$return = array();
	$return['success'] = 1;
	$return['message'] = '任务已过期，不能抢单！';
	die(json_encode($return));
}
if(empty($task) || $task['status'] != 1){
	$return = array();
	$return['success'] = 1;
	$return['message'] = '您来晚了，订单已被抢！';
	die(json_encode($return));
}else{
	$data = array();
	$data['openid'] = $_W['openid'];
	$data['uniacid'] = $_W['uniacid'];
	$data['taskid'] = $id;
	$data['create_time'] = time();
	
	pdo_insert('imeepos_runner3_recive',$data);
	$reciveid = pdo_insertid();
	pdo_update('imeepos_runner3_tasks',array('status'=>2),array('id'=>$id));
	//发送消息
	$content = "";
	$content = "恭喜您，您的任务已被".$runner['nickname']."接单，请注意任务动态~\n";
	$content .= "时间：".date('Y年m月d日 h点i分')."\n";
	$content .= "咚咚咚，您的任务已被受理".$task['code']."，请注意保存，不要泄露~点击详情查看详情";
	$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$task['id']));
	mc_notice_consume2($task['openid'], '您的任务已被受理提醒', $content, $url,'');

	$member = M('member')->getInfo($_W['openid']);
	
	$member['realname'] = !empty($member['realname'])?$member['realname']:$member['nickname'];
	
	$content = "【".$member['realname']."】,受理此任务！联系电话".$member['mobile'];
	$data = array();
	$data['uniacid'] = $_W['uniacid'];
	$data['openid'] = $_W['openid'];
	$data['create_time'] = time();
	$data['taskid'] = $task['id'];
	$data['content'] = $content;
	M('tasks_log')->update($data);
	
	$return = array();
	$return['success'] = 0;
	$return['message'] = '恭喜您，抢单成功！';
	$return['taskid'] = $task['id'];
	$return['id'] = $reciveid;
	die(json_encode($return));
}
