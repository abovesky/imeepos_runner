<?php
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$item = M('setting')->getValue('divider_set');
$return = array();
$return['success'] = 'true';
$return['dgnightconfig'] = array(
    'isnight'=>false,
    'min'=>floatval($item['buy_min_num']),
    'max'=>floatval($item['buy_max_num']),
    'start'=>floatval($item['buy_default_num'])
);
$return['fastaddmoneyconfig'] = array(
    'min'=>intval($item['buy_min_num']),
    'max'=>intval($item['buy_max_num']),
    'start'=>intval($item['buy_default_num'])
);
die(json_encode($return));