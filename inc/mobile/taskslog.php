<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/1
 * Time: 22:18
 */
global $_W,$_GPC;

$member = M('member')->getInfo($_W['openid']);
$content = "【".$member['realname']."】,".$_GPC['content'];
$data = array();

$data['uniacid'] = $_W['uniacid'];
$data['openid'] = $_W['openid'];
$data['create_time'] = time();
$data['taskid'] = intval($_GPC['taskid']);
$data['content'] = $content;
M('tasks_log')->update($data);