<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

load()->func('file');

$data = array();
$data['freight'] = floatval($_GPC['freight']);
$data['title'] = trim($_GPC['title']);
$data['buyprovince'] = urldecode($_GPC['buyprovince']);
$data['buycity'] = urldecode($_GPC['buycity']);

$data['province'] = urldecode($_GPC['province']);
$data['city'] = urldecode($_GPC['city']);
$data['address'] = urldecode($_GPC['address']);

$data['receivelon'] = trim($_GPC['receivelon']);
$data['receivelat'] = trim($_GPC['receivelat']);
$data['expectedtime'] = intval($_GPC['expectedtime']);

$data['buyaddress'] = trim($_GPC['buyaddress']);

$data['sendlon'] = trim($_GPC['sendlon']);
$data['sendlat'] = trim($_GPC['sendlat']);

$data['other'] = trim($_GPC['other']);
$data['distance'] = intval($_GPC['distance']);

$data['receiveaddress'] = trim($_GPC['receiveaddress']);

$data['uniacid'] = $_W['uniacid'];
$data['openid'] = $_W['openid'];

if(!empty($data['expectedtime'])){
	$data['limit_time'] = time()+60*$data['expectedtime'];
}
$distance = floatval($data['distance']/1000);

$buy = $data;

if($buy['title'] != '语音'){
	//语音购买
	//生成声音
	$text = "";
	if(!empty($data['expectedtime'])){
		$text .= "帮我买: ";
		if(!empty($data['buyaddress'])){
			$text .= "\n从:".$data['buyaddress']."\n购买\n";
		}
		
		if(!empty($data['title'])){
			$text .= "\n".$data['title']."\n";
		}
		$text .= "，\n送到:\n".$data['receiveaddress']."\n";
		
		if(!empty($data['distance'])){
			$text .= "总路程:\n".($data['distance']/1000);
		}
		
		$text .= "公里，送达时间:\n".date('h点i分',$data['limit_time']);
		if(!empty($data['other'])){
			$text .= "备注:\n".$data['other']."\n";
		}
		
		if(!empty($data['freight'])){
			$text .= "赏金:\n".$data['freight']."元。";
		}
	}else{
		$text .= "帮我买: ";
		if(!empty($data['buyaddress'])){
			$text .= "\n从:".$data['buyaddress']."\n购买:\n";
		}
		if(!empty($data['title'])){
			$text .= "\n".$data['title']."\n";
		}
		$text .= "，\n送到:\n".$data['receiveaddress']."\n";
		
		if(!empty($data['distance'])){
			$text .= "总路程:\n".($data['distance']/1000)."公里\n";
		}
		
		$text .= "送达时间: \n不限  \n";
		if(!empty($data['other'])){
			$text .= "备注:\n".$data['other']."\n";
		}
		
		if(!empty($data['freight'])){
			$text .= "赏金:\n".$data['freight']."元。";
		}
		
	}
	
	$acc = WeAccount::create();
	if(!empty($text)){
		$url = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=5&text=".urlencode($text);
	
		$img = array();
		$data = file_get_contents($url);
		$type = 'mp3';
		$filename = "audios/imeepos_runner/".time()."_".random(6).".".$type;
		if(file_write($filename,$data)){
			$result = $acc->uploadMedia($filename,'voice');
			$img['media_id'] = $result['media_id'];
		}
	}
	
}else{
	$img = array();
	$img['media_id'] = trim($_GPC['media_id_voice']);
}


//插入任务表
$tasks = array();
$tasks['uniacid'] = $_W['uniacid'];
$tasks['openid'] = $_W['openid'];
$tasks['create_time'] = time();
$tasks['desc'] = $text;
$tasks['city'] = trim($buy['city']);
$tasks['media_id'] = $img['media_id'];
$tasks['status'] = 0;
$tasks['type'] = 2;

pdo_insert('imeepos_runner3_tasks',$tasks);

$buy['taskid'] = pdo_insertid();

$code = random(4,true);
$codetask = array();
$codetask['code'] = $code;
$qrcode = 'imeepos_runner'.md5($code.$tasks['create_time']);
$codetask['qrcode'] = $qrcode;

pdo_update('imeepos_runner3_tasks',$codetask,array('id'=>$buy['taskid']));

pdo_insert('imeepos_runner3_buy',$buy);

//插入订单记录
$paylog = array();
$paylog['fee'] = floatval($buy['freight']);
$paylog['tid'] = "U".time().random(6,true);
$paylog['uniacid'] = $_W['uniacid'];
$paylog['setting'] = iserializer(array('taskid'=>$buy['taskid']));
$paylog['status'] = 0;
$paylog['openid'] = $_W['openid'];
$paylog['time'] = time();
$paylog['type'] = 'post_buy';

pdo_insert('imeepos_runner3_paylog',$paylog);
$tid = pdo_insertid();

//imeepos_runner3_detail
$result = array();
$result['result'] = 0;
$result['paylog'] = $paylog;
$result['media_id'] = $img['media_id'];
if(empty($distance)){
	$result['message'] = '总费用：'.$paylog['fee']."元";
}else{
	$result['message'] = '总路程:'.$distance.'总费用：'.$paylog['fee']."元";
}

$result['tid'] = $tid;


die(json_encode($result));
