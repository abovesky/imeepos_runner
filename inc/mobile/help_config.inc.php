<?php
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$item = M('setting')->getValue('help_set');


$day_start_time = intval($item['day_start_time']);
$day_end_time = intval($item['day_end_time']);

$month=intval(date('m'));
$day=intval(date('d'));
$year=intval(date('Y'));

$day_start_time = mktime($day_start_time,0,0,$month,$day,$year);
$day_end_time = mktime($day_end_time,0,0,$month,$day,$year);

$now = time();
if($now>=$day_start_time && $now <=$day_end_time){
    $return = array();
    $return['success'] = 'true';
    $return['dgnightconfig'] = array(
        'isnight'=>false,
        'min'=>intval($item['day_start']),
        'max'=>intval($item['day_max']),
        'start'=>intval($item['day_start'])
    );
    $return['fastaddmoneyconfig'] = array(
        'min'=>intval($item['day_start']),
        'max'=>intval($item['day_max']),
        'start'=>intval($item['day_start'])
    );

    die(json_encode($return));
}else{
    $return = array();
    $return['success'] = 'true';
    $return['dgnightconfig'] = array(
        'isnight'=>true,
        'min'=>intval($item['night_min']),
        'max'=>intval($item['night_max']),
        'start'=>intval($item['night_min'])
    );
    $return['fastaddmoneyconfig'] = array(
        'min'=>intval($item['night_min']),
        'max'=>intval($item['night_max']),
        'start'=>intval($item['night_min'])
    );
    die(json_encode($return));
}


$return = array();
$return['success'] = 'true';
$return['dgnightconfig'] = array('isnight'=>true,'min'=>20,'max'=>100,'start'=>30);
$return['fastaddmoneyconfig'] = array('min'=>0,'max'=>20,'start'=>0);

die(json_encode($return));