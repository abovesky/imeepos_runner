<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$user = M('member')->getInfo($_W['openid']);
$act = trim($_GPC['act']);

if($act == 'post'){
    if(!empty($user['forbid'])){
        $return = array();
		$return['user'] = $user;
        $return['status'] = -1;
        $return['message'] = '系统检测，您已违反本平台相关规则，已被禁止！请联系管理员解封！';
        $return['url'] = $this->createMobileUrl('home');
        $return['data'] = $runner;
        die(json_encode($return));
    }
	if(empty($user['mobile']) || empty($user['realname'])){
		$return = array();
		$return['user'] = $user;

		$return['status'] = -1;
		$return['message'] = '请完善您的个人信息！';
		$return['url'] = $this->createMobileUrl('home_edit');
		die(json_encode($return));
	}
	$return = array();
	$return['user'] = $user;
	$return['message'] = '请完善您的个人信息！';
	$return['status'] = 0;
	die(json_encode($return));
}

if($act == 'runner'){
	//检查信誉值
	//获取后台设置
	$setting = M('setting')->getValue('v_set');
	
	if($setting['recive_open'] == 0){
		$return = array();
		$return['status'] = 0;
		$return['setting'] = $setting;
		die(json_encode($return));
	}
	
	if(empty($user['isrunner'])){
		$return = array();
		$return['status'] = -1;
		$return['message'] = '您还没有认证您的个人信息，请前往认证！';
		$return['url'] = $this->createMobileUrl('v');
		$return['setting'] = $setting;
		die(json_encode($return));
	}
	if(!empty($user['forbid'])){
		$return = array();
		$return['status'] = 1;
		$return['message'] = '系统检测，您已违反本平台相关规则，已被禁止！请联系管理员解封！';
		$return['url'] = $this->createMobileUrl('home');
		$return['setting'] = $setting;
		$return['data'] = $runner;
		die(json_encode($return));
	}
	if(empty($user['status'])){
		$return = array();
		$return['status'] = 1;
		$return['message'] = '你的信息正在审核，请耐心等待！';
		$return['url'] = $this->createMobileUrl('home');
		$return['setting'] = $setting;
		$return['data'] = $runner;
		die(json_encode($return));
	}
	if(!empty($user['status'])){
		$runner = M('member')->getInfo($_W['openid']);
		$xinyu = intval($runner['xinyu']);
		$kouchu = intval($setting['giveup_num']);
		$shengyu = $xinyu - $kouchu;
		if($shengyu < 0){
			$return = array();
			$return['status'] = 1;
			$return['message'] = '信誉度小于'.$kouchu.',暂时不能接单，请前往充值';
			$return['url'] = $this->createMobileUrl('runner_xinyu');
			$return['data'] = $runner;
			$return['setting'] = $setting;
			die(json_encode($return));
		}
		if($shengyu >= 0 ){
			$return = array();
			$return['status'] = 0;
			$return['url'] = $this->createMobileUrl('runner_xinyu');
			$return['setting'] = $setting;
			die(json_encode($return));
		}
	}
}