<?php
global $_W,$_GPC;

$input = $_GPC;
M('detail');
M('tasks');
load()->func('file');

$data = array();

$data['goodsweight'] = floatval($input['goodsweight']);
$data['goodscost'] = floatval($input['goodscost']);
$data['goodsname'] = trim($input['goodstitle']);

$data['sendprovince'] = '';
$data['sendcity'] = trim($input['sendaddress']['city']);
$data['sendaddress'] = trim($input['sendaddress']['title']);
$data['senddetail'] = trim($input['sendaddress']['detail']);

$data['receiveprovince'] = '';
$data['receivecity'] = trim($input['receiveaddress']['city']);
$data['receiveaddress'] = trim($input['receiveaddress']['title']);
$data['receivedetail'] = trim($input['receiveaddress']['detail']);
$data['receiverealname'] = trim($input['receiveaddress']['realname']);
$data['receivemobile'] = trim($input['receiveaddress']['mobile']);
$data['message'] = trim($input['message']);
$data['sendlon'] = floatval($input['sendaddress']['lng']);
$data['sendlat'] = floatval($input['sendaddress']['lat']);

$data['receivelon'] = floatval($input['receiveaddress']['lng']);
$data['receivelat'] = floatval($input['receiveaddress']['lat']);

$url = "http://apis.map.qq.com/ws/distance/v1/?mode=driving&from={$data['sendlat']},{$data['sendlon']}&to={$data['receivelat']},{$data['receivelon']}&output=json&key=4MHBZ-JVL35-WLMII-Q3NME-3Z2G2-PKBJJ";

load()->func('communication');
$content = ihttp_get($url);

$content = @json_decode($content['content'], true);

if($content['status'] == '373'){
	$result = array();
	$result['result'] = 0;
	$result['message'] = $content['message']."，最大距离为10公里";
	die(json_encode($result));
}

$content = $content['result']['elements'][0];
$data['pickupdate'] = strtotime(trim($input['datatime']['value']));
$data['distance'] = intval(intval($content['distance'])/1000);
$data['float_distance'] = floatval(intval($content['distance'])/1000);

if(!empty($input['datatime']['value'])){
	$data['dataTimeValue'] = strtotime(trim($input['datatime']['value']));//预约取
}else{
	$code = 'plugin_setting';
	$item = M('setting')->getByCode($code);
	$plugin = iunserializer($item['value']);
	$hour = floatval($plugin['limit_time']);
	$data['dataTimeValue'] = time() + intval(60*60*$hour);
}
$data['time'] = intval($input['time']);

if(!pdo_fieldexists('imeepos_runner3_detail','small_money')){
  pdo_query("ALTER TABLE ".tablename('imeepos_runner3_detail')." ADD COLUMN `small_money` float(10,2) DEFAULT '0.00'");
}

$data['small_money'] = floatval($input['small_money']);

//计算费用
$code = 'divider_set';
$sql = "SELECT * FROM ".tablename('imeepos_runner3_setting')." WHERE uniacid = :uniacid AND code = :code";
$params = array(':uniacid'=>$_W['uniacid'],':code'=>$code);
$setting = pdo_fetch($sql,$params);
$set = iunserializer($setting['value']);

//判断是否在起步价内
$distance = floatval($data['distance']);
$max_distance = floatval($set['start_km']);

$price = 0;
$start_price = floatval($set['start_fee']);
if($distance > $max_distance){
	$chao_distance = $distance - $max_distance;
	$limit_km_km = floatval($set['limit_km_km']);
	if($limit_km_km >0){
		$chao = floor($chao_distance / $limit_km_km);
	}else{
		$chao = 0;
	}
	
	$limit_km_fee = floatval($set['limit_km_fee']);
	$price += floatval($chao * $limit_km_fee);
}

$max_goodsweight = floatval($set['start_kg']);
$goodsweight = floatval($data['goodsweight']);

if($goodsweight > $max_goodsweight){
	$chao_goodsweight = $goodsweight - $max_goodsweight;
	$limit_kg_kg = floatval($set['limit_kg_kg']);
	if($limit_kg_kg >0){
		$chao = floor($chao_goodsweight / $limit_kg_kg);
	}else{
		$chao = 0;
	}
	$limit_kg_fee = floatval($set['limit_kg_fee']);
	$price += floatval($chao * $limit_kg_fee);
}

$data['base_fee'] = $start_price;
$data['fee'] = $price;
$data['total'] = $start_price + $price + $data['small_money'];

$now = time();

$month=intval(date('m'));
$day=intval(date('d'));
$year=intval(date('Y'));

$start_time = intval($set['start_time']);
$start_time = mktime($start_time,0,0,$month,$day,$year);
$end_time = intval($set['end_time']);
$end_time = mktime($end_time,0,0,$month,$day,$year);

$time_fee = (1+floatval($set['time_fee'])/100);

if($now >= $start_time && $now <= $end_time){
	
}else{
	$data['base_fee'] = $data['base_fee'];
	$data['fee'] = $data['fee'];
	$data['total'] = $data['total'] * $time_fee;
}

$detail = $data;

$text = "";
if(empty($data['time'])){
	//及时取
	
	$text .="及时取";
	if(!empty($data['small_money'])){
		$text .= "(加急):";
	}
	if(!empty($data['goodsname'])){
		$text .= "商品名称:".$data['goodsname'];
	}
	if(!empty($data['goodscost'])){
		$text .= ",价值：".$data['goodscost']."元";
	}
	if(!empty($data['goodsweight'])){
		$text .= ",".$data['goodsweight']."公斤";
	}
	if(!empty($data['distance'])){
		$text .= ",总路程".($data['distance'])."公里";
	}
	$text .= '从'.$data['sendaddress'].'送到'.$data['receiveaddress'].",赏金：".$data['total']."元";
}else{
	$text .="预约取";
	if(!empty($data['small_money'])){
		$text .= "(加急):";
	}
	if(!empty($data['goodsname'])){
		$text .= "商品名称:".$data['goodsname'];
	}
	if(!empty($data['goodscost'])){
		$text .= ",价值：".$data['goodscost']."元";
	}
	if(!empty($data['goodsweight'])){
		$text .= ",".$data['goodsweight']."公斤";
	}
	if(!empty($data['distance'])){
		$text .= ",总路程".($data['distance'])."公里";
	}
	$text .= '从'.$data['sendaddress'].'送到'.$data['receiveaddress'].",赏金：".$data['total']."元";
}

$acc = WeAccount::create();
if(!empty($text)){
	$url = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=5&text=".urlencode($text);
	$img = array();
	$data = file_get_contents($url);
	$type = 'mp3';
	$filename = "audios/imeepos_runner/".time()."_".random(6).".".$type;
	if(file_write($filename,$data)){
		$result = $acc->uploadMedia($filename,'voice');
		$img['media_id'] = $result['media_id'];
	}
}

$tasks = array();
$tasks['uniacid'] = $_W['uniacid'];
$tasks['openid'] = $_W['openid'];
$tasks['create_time'] = time();
$tasks['desc'] = $text;
$tasks['city'] = trim($input['city']['title']);

$tasks['media_id'] = $img['media_id'];
$tasks['status'] = 0;
$tasks['type'] = intval($detail['time']);

$tasks['total'] = $detail['total'];
$tasks['small_money'] = $detail['small_money'];
$tasks['address'] = $detail['receiveaddress'];

if(!pdo_fieldexists('imeepos_runner3_tasks','message')){
	pdo_query("ALTER TABLE ".tablename('imeepos_runner3_tasks')." ADD COLUMN `message` varchar(320) DEFAULT ''");
}
$tasks['message'] = $detail['message'];
$tasks['limit_time'] = $detail['dataTimeValue'];
pdo_insert('imeepos_runner3_tasks',$tasks);

$detail['taskid'] = pdo_insertid();
$code = random(4,true);
$codetask = array();
$codetask['code'] = $code;
$qrcode = 'imeepos_runner'.md5($code.$tasks['create_time']);
$codetask['qrcode'] = $qrcode;
pdo_update('imeepos_runner3_tasks',$codetask,array('id'=>$detail['taskid']));

pdo_insert('imeepos_runner3_detail',$detail);

//插入订单记录
$paylog = array();
$paylog['fee'] = $detail['total'];
$paylog['tid'] = "U".time().random(6,true);
$paylog['uniacid'] = $_W['uniacid'];
$paylog['setting'] = iserializer(array('taskid'=>$detail['taskid']));
$paylog['status'] = 0;
$paylog['openid'] = $_W['openid'];
$paylog['time'] = time();
$paylog['type'] = 'post_task';

pdo_insert('imeepos_runner3_paylog',$paylog);
$tid = pdo_insertid();

//imeepos_runner3_detail
$result = array();
$result['result'] = 0;
$result['paylog'] = $paylog;
$result['media_id'] = $img['media_id'];
if(empty($distance)){
	$result['message'] = '总费用：'.$detail['total']."元";
}else{
	$result['message'] = '总路程：'.$distance.'公里，总费用：'.$detail['total']."元";
}

$member = M('member')->getInfo($_W['openid']);
$content = "【".$member['nickname']."】,成功发布此任务！";
$data = array();
$data['uniacid'] = $_W['uniacid'];
$data['openid'] = $_W['openid'];
$data['create_time'] = time();
$data['taskid'] = $detail['taskid'];
$data['content'] = $content;
$data['lat'] = $detail['receivelat'];
$data['lng'] = $detail['receivelon'];
M('tasks_log')->update($data);

//新订单后台提醒
$data = array();
$data['uniacid'] = $_W['uniacid'];
$data['create_time'] = time();
$data['status'] = 0;
$data['title'] = "【".$member['nickname']."】成功提交任务";
$data['link'] = '';
M('message')->update($data);

$result['tid'] = $tid;
$result['detail'] = $detail;
$result['content'] = $content;
$tasks['limit_time'] = date('Y-m-d H:i',$tasks['limit_time']);
$result['tasks'] = $tasks;

die(json_encode($result));