<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
load()->model('mc');
$page = 1;
$psize = 20;

$sql = "SELECT * FROM ".tablename('imeepos_runner3_listenlog')." WHERE uniacid = :uniacid AND openid = :openid order by create_time DESC limit ".($page-1)*$psize.",".$psize;
$params = array(':uniacid'=>$_W['uniacid'],':openid'=>$_W['openid']);
$orders = pdo_fetchall($sql,$params);

foreach ($orders as &$order){
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id = :id";
	$params = array(':id'=>$order['taskid']);
	$item = pdo_fetch($sql,$params);
	$uid = mc_openid2uid($item['openid']);
	$order['user'] = mc_fetch($uid);
	$order['text'] = $item['desc'];
	$order['status'] = $item['status'];
	$order['create_time'] = date('Y-m-d h:i',$order['create_time']);
}

$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/runner/listen';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}