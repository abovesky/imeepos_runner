<?php
global $_W,$_GPC;
$taskid = intval($_GPC['taskid']);

if(empty($taskid)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '系统错误';
    die(json_encode($data));
}

$task = M('tasks')->getInfo($taskid);

if(empty($task)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '系统错误';
    die(json_encode($data));
}
$recive = M('recive')->getRecive($taskid);

if($recive['openid'] != $_W['openid']){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '权限错误';
    die(json_encode($data));
}
$poiaddress = trim($_GPC['poiaddress']);
$member = M('member')->getInfo($_W['openid']);
$content = "［".$member['realname']."］我现在在".$poiaddress."\n电话：".$member['mobile'];
$data = array();
$data['uniacid'] = $_W['uniacid'];
$data['openid'] = $_W['openid'];
$data['create_time'] = time();
$data['taskid'] = $taskid;
$data['content'] = $content;
$latlng = $_GPC['latlng'];
$data['lat'] = $latlng['lat'];
$data['lng'] = $latlng['lng'];

M('tasks_log')->update($data);

$data = array();
$data['status'] = 1;
$data['message'] = '提交成功';
die(json_encode($data));