<?php
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';


$act = trim($_GPC['act']);

if($act == 'add'){
	$input = $_GPC['__input'];
	$return = M('address')->updateAddress($input);
	die(json_encode($return));
}

if($act == 'delete'){
	$id = intval($_GPC['id']);
	if(empty($id)){
		message('参数错误',referer(),error);
	}
	$return = M('address')->delete($id);
	die($return);
}

$title = "我的常用地址";

$list = M('address')->getMyAddress(1,$_W['openid']);

$template_content = $template.'/home/address';

include $this->template($template_content);