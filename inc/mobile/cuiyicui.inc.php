<?php
global $_W,$_GPC;

$taskid = intval($_GPC['taskid']);

if(empty($taskid)){
	$data = array();
	$data['status'] = 0;
	$data['message'] = '系统错误';
	die(json_encode($data));
}

$task = M('tasks')->getInfo($taskid);

if(empty($task)){
	$data = array();
	$data['status'] = 0;
	$data['message'] = '系统错误';
	die(json_encode($data));
}

$member = M('member')->getInfo($_W['openid']);
$content = "［".$member['nickname']."］师傅快点吧！我等的花儿都谢了";
$data = array();
$data['uniacid'] = $_W['uniacid'];
$data['openid'] = $_W['openid'];
$data['create_time'] = time();
$data['taskid'] = $taskid;
$data['content'] = $content;

M('tasks_log')->update($data);

$data = array();
$data['status'] = 1;
$data['message'] = '提交成功';
die(json_encode($data));