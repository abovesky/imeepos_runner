<?php 
global $_W,$_GPC;

$template = $this->getTemplate(true);
if($_W['container'] == 'wechat'){
	load()->model('mc');
	$uid = mc_openid2uid($_W['openid']);
	$user = mc_fetch($uid,array('nickname','avatar','realname','mobile','gender','residecity','resideprovince'));
	if(empty($user['nickname'])){
		$user = mc_oauth_userinfo();
	}
	$member = M('member')->getInfo($_W['openid']);
	if(empty($member)){
		$data = array();
		$data['uniacid'] = $_W['uniacid'];
		$data['openid'] = $_W['openid'];
		if(!empty($user['nickname'])){
			$data['nickname'] = $user['nickname'];
		}
		if(!empty($user['avatar'])){
			$data['avatar'] = tomedia($user['avatar']);
		}

		$data['time'] = time();
		$data['gender'] = $user['gender'];
		$data['city'] = $user['residecity'];
		$data['provice'] = $user['resideprovince'];
		$data['status'] = $_W['fans']['follow'];
		$data['uid'] = $uid;
		pdo_insert('imeepos_runner3_member',$data);
	}else{
		$data = array();
		if(!empty($user['nickname'])){
			$data['nickname'] = $user['nickname'];
		}
		if(!empty($user['avatar'])){
			$data['avatar'] = tomedia($user['avatar']);
		}
		$data['time'] = time();
		$data['uid'] = $uid;
		pdo_update('imeepos_runner3_member',$data,array('id'=>$member['id']));
	}
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_member')." WHERE uniacid = :uniacid AND openid = :openid";
	$params = array(':uniacid'=>$_W['uniacid'],':openid'=>$_W['openid']);
	$member = pdo_fetch($sql,$params);
	$user = array_merge($user,$member);
}
$share = array();