<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
load()->model('mc');
$id = intval($_GPC['id']);
if(empty($id)){
    message('订单不存在或已删除',referer(),'success');
}
$order = M('tasks')->getInfo($id);
$marker = "";

if(!empty($order['detail']['receivelat'])){
    $marker .= "eword=".urlencode($order['detail']['receiveaddress'])
        ."&epointx=".$order['detail']['receivelon']
        ."&epointy=".$order['detail']['receivelat'];
}
if(!empty($order['detail']['sendlat'])){
    $marker .= "&sword=".urlencode($order['detail']['sendaddress'])
        ."&spointx=".$order['detail']['sendlon']
        ."&spointy=".$order['detail']['sendlat'];
}

$mapUrl = 'http://apis.map.qq.com/tools/routeplan/'.$marker.'&transport=0?referer=myapp&key=4MHBZ-JVL35-WLMII-Q3NME-3Z2G2-PKBJJ';

if(empty($order)){
    message('订单不存在或已删除',referer(),'success');
}
if(!empty($order['detail']['distance'])){
    $order['detail']['distance_title'] = "总路程：".$order['detail']['distance']."KM";
}
if(floatval($order['detail']['goodsweight'])>0){
    $order['detail']['goodsweight_title'] = $order['detail']['goodsweight'];
}
if(floatval($order['detail']['goodscost'])>0){
    $order['detail']['goodscost_title'] = $order['detail']['goodscost'];
}
if(floatval($order['limit_time'])>0){
    $order['limit_time_title'] = date('Y-m-d H:i',$order['limit_time']);
}
M('tasks')->addReadNum($id);

$where = " AND taskid = {$id}";
$tasks_log = M('tasks_log')->getList(1,$where);
if($_GPC['act'] == 'tasks_log'){
    die(json_encode($tasks_log['list']));
}
if($order['type'] == 0 || $order['type'] == 1){
    $title = '帮我送';
}else if($order['type'] == 2 || $order['type'] == 3){
    $title = '帮我买';
}else{
    $title = '帮忙';
}
$order['create_time'] = date('Y-m-d H:i',$order['create_time']);
$recive = M('recive')->getRecive($id);
if(!empty($recive)){
    if($recive['openid'] != $_W['openid'] && $order['openid'] != $_W['openid'] && $order['status'] != 1){
        message('此订单已被抢或已取消',referer(),'error');
    }
}

$role = 'runner';
if($order['openid'] == $_W['openid']){
    $role = 'owener';
}
if(!empty($recive)){
    if($recive['openid'] == $_W['openid']){
        $role = 'runner';
    }
}
$runner = M('member')->getInfo($recive['openid']);
$user = M('member')->getInfo($order['openid']);
$edit_url = $this->createMobileUrl('post');

if($order['status'] == 4){
    $user_star = M('star')->getStar($id,$order['openid']);
    $recive_star = M('star')->getStar($id,$recive['openid']);
}else{
    $user_star = array();
    $recive_star = array();
}

$template_content = $template.'/task/detail_vux';
include $this->template($template_content);