<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

$type = trim($_GPC['type']);
$setting = M('setting')->getValue('v_set');
$title = '任务大厅';

$where = " AND status = 1";
$advs = M('advs')->getList(1,$where);
$advs = $advs['list'];

$act = trim($_GPC['act']);
$type = trim($_GPC['type']);

$page = !empty($_GPC['page'])?intval($_GPC['page']):1;

$where = " AND status = 1 AND (type = 0 OR type = 1)";
$result = M('tasks')->getList($page,$where);
$orderss1 = $result['list'];
$orders1 = formatOrders($orderss1);

$where = " AND status = 1 AND (type = 2 OR type = 3)";
$result = M('tasks')->getList($page,$where);
$orderss2 = $result['list'];
$orders2 = formatOrders($orderss2);

$where = " AND status = 1 AND (type = 4 OR type = 5)";
$result = M('tasks')->getList($page,$where);
$orderss3 = $result['list'];
$orders3 = formatOrders($orderss3);

if($act == 'more'){
	if($type == 'song'){
		$data = array();
		$data['data'] = $orders1;
		$data['status'] = !empty($orders1)?1:0;
		die(json_encode($data));
	}else{
		$data = array();
		$data['data'] = $orders2;
		$data['status'] = !empty($orders2)?1:0;
		die(json_encode($data));
	}
}
$template_content = $template.'/task/tasks_vux';
include $this->template($template_content);

function formatOrders($orderss){
	$orders = array();
	if(!empty($orderss)){
		foreach($orderss as $order){
			$user = M('member')->getInfo($order['openid']);
			$order['src'] = $user['avatar'];
			$order['nickname'] = cutstr($user['nickname'],'5');
			$order['text'] = $order['desc'];
			$order['status'] = $order['status'];
			if($order['type'] == 0){
				$order['type_title'] = '及时取';
			}else if($order['type'] == 1){
				$order['type_title'] = '预约取';
			}else if($order['type'] == 2){
				$order['type_title'] = '帮我买-语音';
			}else if($order['type'] == 3){
				$order['type_title'] = '帮我买-文字';
			}else if($order['type'] == 4){
				$order['type_title'] = '帮忙-语音';
			}else if($order['type'] == 5){
				$order['type_title'] = '帮忙-文字';
			}

			$order['create_time'] = date('Y-m-d H:i',$order['create_time']);
			$order['limit_time'] = date('Y-m-d H:i',$order['limit_time']);
			$orders[] = $order;
		}
	}
	return $orders;
}