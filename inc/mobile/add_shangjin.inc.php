<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/2
 * Time: 20:48
 */
global $_W,$_GPC;

$taskid = intval($_GPC['taskid']);
$value = floatval($_GPC['value']);

if(empty($taskid)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '参数错误';
    die(json_encode($data));
}
$tasks = M('tasks')->getInfo($taskid);
if(empty($tasks)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '参数错误';
    die(json_encode($data));
}

$data = array();
$data['small_money'] = floatval($tasks['small_money']) + floatval($value);

$data['total'] = floatval($tasks['total']) + floatval($value);
if($tasks['limit_time'] < time()){
    //已过期
    $settingitem = M('setting')->getValue('plugin_setting');
    $hour = intval($settingitem['limit_time']);
    $data['limit_time'] = time() + intval(60*60*$hour);
}
$data['id'] = $taskid;

//插入订单记录
$paylog = array();
$paylog['fee'] = floatval($value);
$paylog['setting'] = iserializer($data);
$paylog['type'] = 'add_shangjin';
$paylog['taskid'] = $taskid;
$paylog = M('paylog')->autoUpdate($paylog);
$tid = $paylog['id'];
//$return = M('tasks')->update($data);

$data = array();
$data['status'] = 1;
$data['tid'] = $tid;
$data['message'] = '增加赏金成功';
die(json_encode($data));