<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

load()->func('file');
$title = "跑腿认证";

//发送消息
$sysms_set = M('setting')->getValue('sms_set');
$item = M('setting')->getValue('v_set');

$act = trim($_GPC['act']);

if($act == 'post'){
	$code = trim($_GPC['code']);
	$codeid = intval($_GPC['codeid']);
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_code')." WHERE id = :id";
	$params = array(':id'=>$codeid);
	$sms = pdo_fetch($sql,$params);
	
	if($code != $sms['code']){
		$return = array();
		$return['status'] = -1;
		$return['code'] = $sms['code'];
		$return['message'] = '您输入的验证码有误！';
		die(json_encode($return));
	}
	$data = array();
	$data['realname'] = trim($_GPC['realname']);
	$data['mobile'] = trim($_GPC['mobile']);
	$data['cardnum'] = trim($_GPC['cardnum']);
	
	if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $_GPC['card_image1'], $result)){
		$type = $result[2];
		$filename = "images/imeepos_runner/".time()."_".random(6).".".$type;
		if(file_write($filename, base64_decode(str_replace($result[1],'',$_GPC['card_image1'])))){
			$data['card_image1'] = tomedia($filename);
		}
	}else{
		$data['card_image1'] = tomedia($_GPC['card_image1']);
	}
	
	if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $_GPC['card_image2'], $result)){
		$type = $result[2];
		$filename = "images/imeepos_runner/".time()."_".random(6).".".$type;
		if(file_write($filename, base64_decode(str_replace($result[1],'',$_GPC['card_image2'])))){
			$data['card_image2'] = tomedia($filename);
		}
	}else{
		$data['card_image2'] = tomedia($_GPC['card_image2']);
	}
	
	pdo_update('imeepos_runner3_member',$data,array('openid'=>$_W['openid'],'uniacid'=>$_W['uniacid']));
	
	$item = M('setting')->getValue('v_set');
	$fee = floatval($item['runner_money']);
	$xinyu = intval($item['start_num']);
	//插入订单记录
	$paylog = array();
	$paylog['fee'] = $fee;
	$paylog['tid'] = "U".time().random(6,true);
	$paylog['uniacid'] = $_W['uniacid'];
	$paylog['setting'] = iserializer(array('openid'=>$_W['openid'],'xinyu'=>$xinyu));
	$paylog['status'] = 0;
	$paylog['openid'] = $_W['openid'];
	$paylog['time'] = time();
	$paylog['type'] = 'runner';
	pdo_insert('imeepos_runner3_paylog',$paylog);
	$tid = pdo_insertid();
	//imeepos_runner3_detail
	$result = array();
	$result['status'] = 0;
	$result['message'] = '认证资料提交成功，请前往支付认证费用！总费用：'.$fee."元";
	$result['tid'] = $tid;
	die(json_encode($result));
}
$user = M('member')->getInfo($_W['openid']);
$_pjax = trim($_GPC['_pjax']);
$template_content = $template.'/runner/v';
include $this->template($template_content);