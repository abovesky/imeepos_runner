<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$data = array();
$data['freight'] = floatval($_GPC['freight']);
$data['title'] = trim($_GPC['title']);

$data['province'] = urldecode($_GPC['province']);
$data['city'] = urldecode($_GPC['city']);
$data['address'] = urldecode($_GPC['address']);

$data['voicetime'] = intval($_GPC['voicetime']);

$data['expectedtime'] = intval($_GPC['expectedtime']);
$data['media_id_voice'] = $_GPC['media_id_voice'];

$data['receivelon'] = trim($_GPC['receivelon']);
$data['receivelat'] = trim($_GPC['receivelat']);

$data['distance'] = intval($_GPC['distance']);
$data['receiveaddress'] = trim($_GPC['receiveaddress']);