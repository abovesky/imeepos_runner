<?php
ini_set('display_errors', '1');
error_reporting(E_ALL ^ E_NOTICE);

global $_W,$_GPC;
$taskid = intval($_GPC['taskid']);
$star = intval($_GPC['star']);
$desc = trim($_GPC['desc']);

if(empty($taskid)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '任务ID不能为空';
    die(json_encode($data));
}

$tasks = M('tasks')->getInfo($taskid);
if($tasks['status'] >= 4){
    $recive = M('recive')->getRecive($taskid);
    if($tasks['openid'] == $_W['openid']){
        //任务住评价
        $data = array();
        $data['taskid'] = $taskid;
        $data['from_openid'] = $_W['openid'];
        $data['to_openid'] = $recive['openid'];
        $data['star'] = $star;
        $data['type'] = !empty($star)?'1':'2';//跑腿首评
        $data['content'] = $desc;
        $data['create_time'] = time();
        M('star')->update($data);
        $data = array();
        $data['status'] = 1;
        $data['message'] = '提交成功';
        die(json_encode($data));
    }
    if($recive['openid'] == $_W['openid']){
        $data = array();
        $data['uniacid'] = $_W['uniacid'];
        $data['taskid'] = $taskid;
        $data['from_openid'] = $_W['openid'];
        $data['to_openid'] = $tasks['openid'];
        $data['star'] = $star;
        $data['type'] = !empty($star)?'3':'4';//用户首评
        $data['content'] = $desc;
        $data['create_time'] = time();
        M('star')->update($data);
        $data = array();
        $data['status'] = 1;
        $data['message'] = '提交成功';
        die(json_encode($data));
    }
    $data = array();
    $data['status'] = 0;
    $data['message'] = '参数错误';
    die(json_encode($data));
}else{
    $data = array();
    $data['status'] = 0;
    $data['message'] = '任务尚未成功';
    die(json_encode($data));
}
