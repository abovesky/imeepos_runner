<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$city = trim($_GPC['city']);
$province = trim($_GPC['province']);

$city = str_replace('市', '', $city);
$province = str_replace('省','',$province);

if(!empty($city)){
	pdo_update('imeepos_runner3_member',array('city'=>$city),array('openid'=>$_W['openid'],'uniacid'=>$_W['uniacid']));
}
if(!empty($province)){
	pdo_update('imeepos_runner3_member',array('provice'=>$province),array('openid'=>$_W['openid'],'uniacid'=>$_W['uniacid']));
}
if(!empty($city)){
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id NOT IN (SELECT taskid FROM ".tablename('imeepos_runner3_listenlog')." WHERE uniacid = '{$_W['uniacid']}' AND openid = '{$_W['openid']}') AND uniacid = '{$_W['uniacid']}' AND status =:status order by create_time DESC limit 1";
	$params = array(':status'=>1);
	$item = pdo_fetch($sql,$params);
}else{
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id NOT IN (SELECT taskid FROM ".tablename('imeepos_runner3_listenlog')." WHERE uniacid = '{$_W['uniacid']}' AND openid = '{$_W['openid']}') AND uniacid = :uniacid AND status =:status order by create_time DESC limit 1";
	$params = array(':uniacid'=>$_W['uniacid'],':status'=>1);
	$item = pdo_fetch($sql,$params);
}

load()->model('mc');
if(empty($item)){
	$data = array();
	$data['status'] = 1;
	die(json_encode($data));
}else{
	//插入播放记录
	$data = array();
	$data['uniacid'] = $_W['uniacid'];
	$data['openid'] = $_W['openid'];
	$data['taskid'] = $item['id'];
	$data['create_time'] = time();
	$data['openid'] = $_W['openid'];
	
	pdo_insert('imeepos_runner3_listenlog',$data);
	
	$uid = mc_openid2uid($item['openid']);
	$user = mc_fetch($uid);
	
	$data = array();
	$data['status'] = 0;
	$data['avatar'] = tomedia($user['avatar']);
	$data['nickname'] = $user['nickname'];
	$data['desc'] = $item['desc'];
	$data['media_id'] = $item['media_id'];
	$data['type'] = $item['type'];
	$data['data'] = $item;
	die(json_encode($data));
}