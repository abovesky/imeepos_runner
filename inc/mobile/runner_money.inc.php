<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

$item = M('setting')->getByCode('v_set');
$v_set = iunserializer($item['value']);

$title = "我的佣金";
load()->model('mc');

$user = mc_fetch($_W['openid']);

$sql = "SELECT * FROM ".tablename('imeepos_runner3_moneylog')." WHERE openid = :openid ORDER BY create_time DESC ";

$params = array(':openid'=>$_W['openid']);
$lists = pdo_fetchall($sql,$params);

foreach ($lists as $li){
	$li['avatar'] = $user['avatar'];
	$li['create_time'] = date('m-d h:i',$li['create_time']);
	$list[] = $li;
}

$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/runner/money';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}