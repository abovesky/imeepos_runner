<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/6/2
 * Time: 22:01
 */

global $_W,$_GPC;

$taskid = intval($_GPC['taskid']);
$value = floatval($_GPC['value']);

if(empty($taskid)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '参数错误';
    die(json_encode($data));
}

$tasks = M('tasks')->getInfo($taskid);

if(empty($tasks)){
    $data = array();
    $data['status'] = 0;
    $data['message'] = '参数错误';
    die(json_encode($data));
}

$data = array();
$data['id'] = $taskid;
if($tasks['limit_time'] < time()){
    $data['limit_time'] = time() + intval(60*60*$value);
}else{
    $data['limit_time'] = $tasks['limit_time'] + intval(60*60*$value);
}


$return = M('tasks')->update($data);

$data = array();
$data['status'] = 1;
$data['tid'] = $tid;
$data['message'] = '延时成功';
die(json_encode($data));
