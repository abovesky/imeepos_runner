<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
load()->model('mc');

$title = "我的订单";

$where = "";
$params = array(':openid'=>$_W['openid'],':uniacid'=>$_W['uniacid']);
if(isset($_GPC['status'])){
	$status = intval($_GPC['status']);
	$where .= " AND status = :status";
	$params[':status'] = $status;
}else{
	$where .= " AND status > :status";
	$params[':status'] = 0;
}

$page = 1;
$psize = 20;
$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE openid = :openid AND uniacid = :uniacid {$where} ORDER BY create_time DESC limit ".($page-1)*$psize.",".$psize;

$orders = pdo_fetchall($sql,$params);
foreach ($orders as &$order){
	$order['user'] = mc_fetch($order['openid']);
	$order['create_time'] = date('Y-m-d h:i',$order['create_time']);
}
unset($order);

$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/home/order';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}