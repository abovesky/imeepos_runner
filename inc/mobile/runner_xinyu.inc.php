<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

$title = "信誉度充值";

$act = trim($_GPC['act']);
if($act == 'post'){
	$data = array();
	$num = intval($_GPC['num']);
	if(empty($num) || $num <= 0){
		die(json_encode(array('message'=>'充值金额不能小于等于0','status'=>0)));
	}
	//计算金钱
	$table = "imeepos_runner3_setting";
	$code = 'v_set';
	
	//pdo_delete($table);
	
	$sql = "SELECT * FROM ".tablename($table)." WHERE uniacid = :uniacid AND code = :code";
	$params = array(':uniacid'=>$_W['uniacid'],':code'=>$code);
	$setting = pdo_fetch($sql,$params);
	
	$item = iunserializer($setting['value']);
	
	if(empty($item['one_money'])){
		$item['one_money'] = 1;
	}
	$one_money = floatval($item['one_money']);
	
	$fee = $one_money * $num;
	$paylog = array();
	$paylog['fee'] = $fee;
	$paylog['tid'] = "U".time().random(6,true);
	$paylog['uniacid'] = $_W['uniacid'];
	$paylog['setting'] = iserializer(array('num'=>$num));
	$paylog['status'] = 0;
	$paylog['openid'] = $_W['openid'];
	$paylog['time'] = time();
	$paylog['type'] = 'payxinyu';

	pdo_insert('imeepos_runner3_paylog',$paylog);
	$tid = pdo_insertid();
	
	die(json_encode(array('tid'=>$tid,'status'=>1)));
}


$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/runner/xinyu';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}