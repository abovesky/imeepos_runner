<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$title = "完成订单";
//订单编号
$id = intval($_GPC['id']);
load()->model('mc');

$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE id = :id";
$params = array(':id'=>$id);
$task = pdo_fetch($sql,$params);

if(!empty($task)){
	if($task['status'] >= 2){
		$return = array();
		$return['result'] =2;
		$return['message'] = '此订单已有人接单，不可删除！';
		die(json_encode($return));
	}else if($task['status'] == 1){
		$fee = floatval($task['total']);
		//判断任务是否到期 如果到期则不扣费用 如果未到期扣除费用作为惩罚
		$uid = mc_openid2uid($task['openid']);
		mc_credit_update($uid, 'credit2',$fee,array($uid, '撤销任务退换', 0, 0));
		//应退金额 $fee
		pdo_delete('imeepos_runner3_tasks',array('id'=>$id));
		pdo_delete('imeepos_runner3_recive',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_detail',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_buy',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_listenlog',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_tasks_log',array('taskid'=>$id));
		
		$return = array();
		$return['result'] =0;
		$return['message'] = '恭喜您，操作成功！退换金额'.$fee.'元已余额到账';
		$return['detail'] = $detail;
		$return['task'] = $task;
		die(json_encode($return));
	}else{
		pdo_delete('imeepos_runner3_tasks',array('id'=>$id));
		pdo_delete('imeepos_runner3_recive',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_detail',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_buy',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_listenlog',array('taskid'=>$id));
		pdo_delete('imeepos_runner3_tasks_log',array('taskid'=>$id));

		$return = array();
		$return['result'] =0;
		$return['message'] = '此任务尚未支付，已直接清除';
		die(json_encode($return));
	}
}else{
	$return = array();
	$return['result'] =3;
	$return['message'] = '订单不存在或已删除！';
	die(json_encode($return));
}