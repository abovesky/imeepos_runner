<?php 
global $_W,$_GPC;

include MODULE_ROOT.'/inc/mobile/__init.php';
$title = "资料完善";
$act = trim($_GPC['act']);

$user = M('member')->getInfo($_W['openid']);
if($act == 'post'){
	$data = array();
	$data['realname'] = trim($_GPC['realname']);
	$data['mobile'] = trim($_GPC['mobile']);
	$data['avatar'] = M('image')->createImage($_GPC['avatar']);
	$data['nickname'] = trim($_GPC['nickname']);
	pdo_update('imeepos_runner3_member',$data,array('uid'=>$_W['member']['uid'],'uniacid'=>$_W['uniacid']));
	$user = M('member')->getInfo($_W['member']['uid']);
	$return = array();
	$return['result'] = 0;
	$return['data'] = $user;
	die(json_encode($return));
}
$_pjax = trim($_GPC['_pjax']);

$template_content = $template.'/home/edit';

if($_W['isajax']){
	include $this->template($template_content,TEMPLATE_FETCH);
}else{
	include $this->template($template.'/index');
}