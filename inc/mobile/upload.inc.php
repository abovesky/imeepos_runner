<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';

load()->func('file');

$data = array();
$data['goodsweight'] = floatval($_GPC['goodsweight']);
$data['goodscost'] = floatval($_GPC['goodscost']);
$data['goodsname'] = trim($_GPC['goodsname']);

$data['sendprovince'] = trim($_GPC['sendprovince']);
$data['sendcity'] = trim($_GPC['sendcity']);
$data['sendaddress'] = trim($_GPC['sendaddress']);
$data['receiveprovince'] = trim($_GPC['receiveprovince']);
$data['receivecity'] = trim($_GPC['receivecity']);
$data['receiveaddress'] = trim($_GPC['receiveaddress']);

$data['pickupdate'] = strtotime(trim($_GPC['pickupdate']));//及时去

$data['sendlon'] = floatval($_GPC['sendlon']);
$data['sendlat'] = floatval($_GPC['sendlat']);
$data['receivelon'] = floatval($_GPC['receivelon']);
$data['receivelat'] = floatval($_GPC['receivelat']);
$data['distance'] = intval($_GPC['distance']);

$data['dataTimeValue'] = strtotime(trim($_GPC['dataTimeValue']));//预约取

$data['time'] = intval($_GPC['time']);

//计算费用
$code = 'divider_set';
$sql = "SELECT * FROM ".tablename('imeepos_runner3_setting')." WHERE uniacid = :uniacid AND code = :code";
$params = array(':uniacid'=>$_W['uniacid'],':code'=>$code);
$setting = pdo_fetch($sql,$params);
$set = iunserializer($setting['value']);

//判断是否在起步价内
$distance = floatval($data['distance']/1000);
$max_distance = floatval($set['start_km']);

$price = 0;
$start_price = floatval($set['start_fee']);
if($distance > $max_distance){
	$chao_distance = $distance - $max_distance;
	$limit_km_km = floatval($set['limit_km_km']);
	if($limit_km_km >0){
		$chao = floor($chao_distance / $limit_km_km);
	}else{
		$chao = 0;
	}
	
	$limit_km_fee = floatval($set['limit_km_fee']);
	$price += floatval($chao * $limit_km_fee);
}

$max_goodsweight = floatval($set['start_kg']);
$goodsweight = floatval($data['goodsweight']);

if($goodsweight > $max_goodsweight){
	$chao_goodsweight = $goodsweight - $max_goodsweight;
	$limit_kg_kg = floatval($set['limit_kg_kg']);
	if($limit_kg_kg >0){
		$chao = floor($chao_goodsweight / $limit_kg_kg);
	}else{
		$chao = 0;
	}
	$limit_kg_fee = floatval($set['limit_kg_fee']);
	$price += floatval($chao * $limit_kg_fee);
}

$data['base_fee'] = $start_price;
$data['fee'] = $price;
$data['total'] = $start_price + $price;

$now = time();

$month=intval(date('m'));
$day=intval(date('d'));
$year=intval(date('Y'));

$start_time = intval($set['start_time']);
$start_time = mktime($start_time,0,0,$month,$day,$year);
$end_time = intval($set['end_time']);
$end_time = mktime($end_time,0,0,$month,$day,$year);

$time_fee = (1+floatval($set['time_fee'])/100);

if($now >= $start_time && $now <= $end_time){
	
}else{
	$data['base_fee'] = $data['base_fee'] * $time_fee;
	$data['fee'] = $data['fee'] * $time_fee;
	$data['total'] = $data['total'] * $time_fee;
}

$detail = $data;

$text = "";
if(empty($data['time'])){
	//及时取
	$text .= "及时取：".$data['goodsname']."，价值：".$data['goodscost']."元,".$data['goodsweight']."公斤,总路程".($data['distance']/1000)."公里";
	$text .= '从'.$data['sendaddress'].'送到'.$data['receiveaddress'].",赏金：".$data['total']."元";
}else{
	$text .= "预约取：预约时间".date('m月d日 h点i分',$data['dataTimeValue'])."，商品名称：".$data['goodsname'].",价值：".$data['goodscost']."元,".$data['goodsweight']."公斤,总路程".($data['distance']/1000)."公里\n";
	$text .= '从'.$data['sendaddress'].'送到'.$data['receiveaddress'].",赏金：".$data['total']."元";
}

$acc = WeAccount::create();
if(!empty($text)){
	$url = "http://tts.baidu.com/text2audio?lan=zh&ie=UTF-8&spd=5&text=".urlencode($text);
	$img = array();
	$data = file_get_contents($url);
	$type = 'mp3';
	$filename = "audios/imeepos_runner/".time()."_".random(6).".".$type;
	if(file_write($filename,$data)){
		$result = $acc->uploadMedia($filename,'voice');
		$img['media_id'] = $result['media_id'];
	}
}

$tasks = array();
$tasks['uniacid'] = $_W['uniacid'];
$tasks['openid'] = $_W['openid'];
$tasks['create_time'] = time();
$tasks['desc'] = $text;
$tasks['city'] = trim($_GPC['sendcity']);
$tasks['media_id'] = $img['media_id'];
$tasks['status'] = 0;
$tasks['type'] = intval($_GPC['time']);

pdo_insert('imeepos_runner3_tasks',$tasks);

$detail['taskid'] = pdo_insertid();
$code = random(4,true);
$codetask = array();
$codetask['code'] = $code;
$qrcode = 'imeepos_runner'.md5($code.$tasks['create_time']);
$codetask['qrcode'] = $qrcode;

pdo_update('imeepos_runner3_tasks',$codetask,array('id'=>$detail['taskid']));

pdo_insert('imeepos_runner3_detail',$detail);

//插入订单记录
$paylog = array();
$paylog['fee'] = $detail['total'];
$paylog['tid'] = "U".time().random(6,true);
$paylog['uniacid'] = $_W['uniacid'];
$paylog['setting'] = iserializer(array('taskid'=>$detail['taskid']));
$paylog['status'] = 0;
$paylog['openid'] = $_W['openid'];
$paylog['time'] = time();
$paylog['type'] = 'post_task';

pdo_insert('imeepos_runner3_paylog',$paylog);
$tid = pdo_insertid();

//imeepos_runner3_detail
$result = array();
$result['result'] = 0;
$result['paylog'] = $paylog;
$result['media_id'] = $img['media_id'];
if(empty($distance)){
	$result['message'] = '总费用：'.$detail['total']."元";
}else{
	$result['message'] = '总路程：'.$distance.'公里，总费用：'.$detail['total']."元";
}

$result['tid'] = $tid;


die(json_encode($result));
