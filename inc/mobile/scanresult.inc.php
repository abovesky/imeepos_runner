<?php 
global $_W,$_GPC;
include MODULE_ROOT.'/inc/mobile/__init.php';
$content = $this->message['content'];
include MODULE_ROOT.'/inc/mobile/common/global.func.php';

$qrcode = trim($_GPC['result']);
load()->model('mc');
if(strpos($qrcode,'imeepos_runner') == 0){
	$sql = "SELECT * FROM ".tablename('imeepos_runner3_tasks')." WHERE qrcode = :qrcode";
	$params = array(':qrcode'=>$qrcode);
	$tasks = pdo_fetch($sql,$params);

	if(!empty($tasks)){


		if($tasks['status'] == 1){
			$data = array();
			$data['status'] = 0;
			$data['message'] = '抱歉，此订单还没有人接哦，完成失败！';
			die(json_encode($data));
		}


		if($tasks['status'] == 0){
			$data = array();
			$data['status'] = 0;
			$data['message'] = '抱歉，此订单尚未完成支付，完成失败！';
			die(json_encode($data));
		}


		if($tasks['status'] == 4){
			$data = array();
			$data['status'] = 0;
			$data['message'] = '抱歉，此二维码已失效，完成失败！';
			die(json_encode($data));
		}

		$sql = "SELECT * FROM ".tablename('imeepos_runner3_recive')." WHERE taskid = :taskid";
		$params = array(':taskid'=>$tasks['id']);
		$recive = pdo_fetch($sql,$params);


		if(empty($recive)){
			$data = array();
			$data['status'] = 0;
			$data['message'] = '接单人信息错误，完成失败！';
			die(json_encode($data));
		}


		if($_W['openid'] != $recive['openid']){
			$data = array();
			$data['status'] = 0;
			$data['message'] = '权限错误，这不是您的单子，请确认！';
			die(json_encode($data));
		}
		$fee = floatval($tasks['total']);

		//扣除平台佣金
		$plugin_setting = M('setting')->getValue('plugin_setting');
		$platform_money = intval($plugin_setting['platform_money']);
		if(empty($platform_money) || $platform_money > 100){
			$platform_money = 100;
		}
		$fee = $fee * ($platform_money / 100 );
		
		$uid = mc_openid2uid($recive['openid']);

		mc_credit_update($uid, 'credit2',$fee,array($uid, '跑腿佣金', 0, 0));
		//插入记录表
		$data = array();
		$data['uniacid'] = $_W['uniacid'];
		$data['openid'] = $_W['openid'];
		$data['create_time'] = time();
		$data['reciveid'] = $recive['id'];
		$data['fee'] = $fee;

		$sql = "SELECT * FROM ".tablename('imeepos_runner3_moneylog')." WHERE reciveid = :reciveid";
		$params = array(':reciveid'=>$recive['id']);
		$m = pdo_fetch($sql,$params);

		if(!empty($m)){
			//赏金到账通知
			$content = "";
			$content = "恭喜您，".$fee."赏金已到账！~\n";
			$content .= "时间：".date('Y年m月d日 h点i分')."\n";
			$content .= "咚咚咚，恭喜您，恭喜您，任务完成!赏金".$fee."元已到账余额，请注意查收~，点击继续赚钱";
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$tasks['id']));
			$retrun = mc_notice_consume2($_W['openid'], '赏金到账通知', $content, $url,'');
			$data = array();
			$data['status'] = 1;
			$data['message'] = '恭喜您，任务完成!赏金'.$fee.'元已到账余额，请注意查收';
			die(json_encode($data));
		}else{
			$content = "";
			$content = "恭喜您，".$fee."赏金已到账！~\n";
			$content .= "时间：".date('Y年m月d日 h点i分')."\n";
			$content .= "咚咚咚，恭喜您，任务完成!赏金".$fee."元已到账余额，请注意查收~，点击继续赚钱";
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$tasks['id']));
			$retrun = mc_notice_consume2($_W['openid'], '赏金到账通知', $content, $url,'');
			pdo_update('imeepos_runner3_tasks',array('status'=>4),array('id'=>$tasks['id']));
			pdo_insert('imeepos_runner3_moneylog',$data);

			$content = "";
			$content = "恭喜您，任务完成，请及时互评！~\n";
			$content .= "时间：".date('Y年m月d日 h点i分')."\n";
			$content .= "咚咚咚，您发布的任务已完成，请点击前往评价！";
			$url = $_W['siteroot'].'app/'.$this->createMobileUrl('detail',array('id'=>$tasks['id']));
			$retrun = mc_notice_consume2($tasks['openid'], '任务完成通知', $content, $url,'');
			
			$data = array();
			$data['status'] = 1;
			$data['message'] = '恭喜您，任务完成!赏金'.$fee.'元已到账余额，请注意查收';
			die(json_encode($data));
		}
	}else{
		$data = array();
		$data['status'] = 0;
		$data['message'] = '任务不存在或已删除';
		die(json_encode($data));
	}
}


$data = array();
$data['status'] = 0;
$data['message'] = '任务不存在或已删除';
die(json_encode($data));
?>